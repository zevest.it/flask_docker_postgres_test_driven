import os
from flask.cli import FlaskGroup
from flask import jsonify, request, send_from_directory
from typing import List
from sqlalchemy.orm.exc import NoResultFound
from werkzeug.utils import secure_filename

from project import app, db
from project.models import User, Tweet, Like, Follower, Media

cli = FlaskGroup(app)


@app.route("/")
def hello_world():
    return jsonify(hello="world")


@cli.command("create_db")
def create_db():
    db.drop_all()
    db.create_all()
    objects = []
    users = [User(api_key=f"test_user{i}", name=f"Test_name_{i}") for i in range(1, 4)]
    users.append(User(api_key="test", name="test"))
    tweets = [Tweet(tweet_data=f"tweet{i} text", tweet_media_ids="1, 2, 3", author_id=i) for i in range(1, 4)]
    objects.extend(users + tweets)
    db.session.bulk_save_objects(objects)
    db.session.commit()


def user_info(user):
    followers = db.session.query(Follower).filter(Follower.user_to_follow == user.id).all()
    following = db.session.query(Follower).filter(Follower.follower == user.id).all()
    user_out = dict()
    user_out["id"] = user.id
    user_out["name"] = user.name
    user_out["followers"] = []
    for person in followers:
        person_dict = {"id": db.session.query(User).get(person.follower).id,
                       "name": db.session.query(User).get(person.follower).name}
        user_out["followers"].append(person_dict)
    user_out["following"] = []
    for person in following:
        person_dict = {"id": db.session.query(User).get(person.user_to_follow).id,
                       "name": db.session.query(User).get(person.user_to_follow).name}
        user_out["following"].append(person_dict)
    return user_out


@app.route("/api/users", methods=['POST'])
def user_create():
    """new user creation"""
    api_key = request.form.get('api_key', type=str)
    name = request.form.get('name', type=str)
    # surname = request.form.get('surname', type=str)
    new_user = User(api_key=api_key, name=name)
    db.session.add(new_user)
    db.session.commit()
    return 'user created ok', 201


@app.route("/api/users", methods=['GET'])
def get_users_handler():
    """getting list of users"""
    users: List[User] = db.session.query(User).all()
    users_list = [c.to_json() for c in users]
    return jsonify(users_list), 200


@app.route("/api/users/me", methods=['GET'])
def user_info_me():
    api_key = request.form.get("api_key")
    try:
        user_to_show = db.session.query(User).filter(User.api_key == api_key).one()
        return jsonify(result="true", user=user_info(user_to_show))
    except NoResultFound:
        return jsonify(result="false", error_type="NoResultFound", error_message="no user found")


@app.route("/api/users/<int:user_id>", methods=['GET'])
def user_info_id(user_id: int):
    try:
        user_to_show = db.session.query(User).filter(User.id == user_id).one()
        return jsonify(result="true", user=user_info(user_to_show))
    except NoResultFound:
        return jsonify(result="false", error_type="NoResultFound", error_message="no user found")


@app.route("/api/tweets", methods=['GET'])
def get_tweets_list():
    """getting all tweets"""
    tweets: List[Tweet] = db.session.query(Tweet).all()
    tweets_out = []
    for tweet in tweets:
        curr_tweet = dict()
        curr_tweet["id"] = tweet.id
        curr_tweet["content"] = tweet.tweet_data
        curr_tweet["attachments"] = tweet.tweet_media_ids
        curr_tweet["author"] = dict()
        curr_tweet["author"]["id"] = tweet.author_id
        curr_tweet["author"]["name"] = db.session.query(User).get(tweet.author_id).name
        likes = []
        curr_tweet_likes = db.session.query(Like).filter(Like.tweet_id == tweet.id).all()
        for like in curr_tweet_likes:
            curr_like = dict()
            curr_like["user_id"] = like.author_id
            curr_like_author = db.session.query(User).get(like.author_id)
            curr_like["name"] = curr_like_author.name
            likes.append(curr_like)
        curr_tweet["likes"] = likes
        tweets_out.append(curr_tweet)
    return jsonify(result="true", tweets=tweets_out)


@app.route("/api/tweets", methods=['POST'])
def tweet_create():
    """new tweet creation"""
    api_key = request.form.get("api_key")
    try:
        author = db.session.query(User).filter(User.api_key == api_key).one()
        tweet_data = request.form.get("tweet_data", type=str)
        tweet_media_ids = request.form.get("tweet_media_ids")
        new_tweet = Tweet(tweet_data=tweet_data, author_id=author.id, tweet_media_ids=tweet_media_ids)
        db.session.add(new_tweet)
        db.session.commit()
        db.session.refresh(new_tweet)
        return jsonify(result="true", tweed_id=new_tweet.id), 201
    except NoResultFound:
        return jsonify(result="false", error_type="NoResultFound", error_message="no user found")


@app.route("/api/tweets/<int:tweet_id>", methods=['DELETE'])
def delete_tweet_by_id(tweet_id: int):
    """deleting tweet belonging to requesting user"""
    api_key = request.form.get("api_key")
    try:
        author = db.session.query(User).filter(User.api_key == api_key).one()
        try:
            tweet = db.session.query(Tweet).filter(Tweet.id == tweet_id).one()
            if tweet.author_id == author.id:
                db.session.delete(tweet)
                db.session.commit()
                return jsonify(result="true"), 200
            else:
                return jsonify(result="false", error_type="NoResultFound",
                               error_message="this tweet belongs to another author")
        except NoResultFound:
            return jsonify(result="false", error_type="NoResultFound", error_message="no tweet found")
    except NoResultFound:
        return jsonify(result="false", error_type="NoResultFound", error_message="no user found")


@app.route("/api/tweets/<int:tweet_id>/likes", methods=['GET', 'POST', 'DELETE'])
def tweet_likes(tweet_id: int):
    """adding and deleting like"""
    api_key = request.form.get("api_key")
    try:
        tweet = db.session.query(Tweet).filter(Tweet.id == tweet_id).one()
        if request.method == 'POST':
            try:
                author = db.session.query(User).filter(User.api_key == api_key).one()
                like = db.session.query(Like).filter(Like.tweet_id == tweet.id,
                                                     Like.author_id == author.id).one()
                return jsonify(result="false", error_type="DuplicationError",
                               error_message="you've already added like to this tweet")
            except NoResultFound:
                new_like = Like(tweet_id=tweet.id, author_id=author.id)
                db.session.add(new_like)
                db.session.commit()
                return jsonify(result="true"), 200
        elif request.method == 'DELETE':
            try:
                author = db.session.query(User).filter(User.api_key == api_key).one()
                like = db.session.query(Like).filter(Like.tweet_id == tweet.id,
                                                     Like.author_id == author.id).one()
                db.session.delete(like)
                db.session.commit()
                return jsonify(result="true", status_code=200)
            except NoResultFound:
                return jsonify(result="false", error_type="NoResultFound", error_message="no likes found")
        elif request.method == 'GET':
            likes = db.session.query(Like).filter(Like.tweet_id == tweet.id).all()
            likes_list = [c.to_json() for c in likes]
            return jsonify(likes_list), 200
    except NoResultFound:
        return jsonify(result="false", error_type="NoResultFound", error_message="no tweet found")


@app.route("/api/users/<int:user_id>/follow", methods=['POST', 'DELETE'])
def follow_user(user_id: int):
    api_key = request.form.get("api_key")
    user_to_follow = db.session.query(User).get(user_id)
    if user_to_follow:
        try:
            follower = db.session.query(User).filter(User.api_key == api_key).one()
            try:
                if_follows = db.session.query(Follower).filter(
                    Follower.follower == follower.id,
                    Follower.user_to_follow == user_to_follow.id).one()
                if request.method == 'POST':
                    return jsonify(result="false", error_type="DuplicationError",
                                   error_message="you already follow this user")
                elif request.method == 'DELETE':
                    db.session.delete(if_follows)
                    db.session.commit()
                    return jsonify(result="true")
            except NoResultFound:
                new_follower = Follower(follower=follower.id, user_to_follow=user_to_follow.id)
                db.session.add(new_follower)
                db.session.commit()
                return jsonify(result="true")
        except NoResultFound:
            return jsonify(result="false", error_type="NoResultFound", error_message="no follower found")
    else:
        return jsonify(result="false", error_type="NoResultFound", error_message="no user to follow found")


# @app.route("/static/<path:filename>")
# def staticfiles(filename):
#     return send_from_directory(app.config["STATIC_FOLDER"], filename)

@app.route("/<path:filename>")
def staticfiles(filename):
    return send_from_directory(app.config["STATIC_FOLDER"], filename)


@app.route("/media/<path:filename>")
def mediafiles(filename):
    return send_from_directory(app.config["MEDIA_FOLDER"], filename)


@app.route("/api/medias", methods=["GET", "POST"])
def upload_file():
    api_key = request.form.get("api_key")
    for file in request.files:
        file_name = secure_filename(request.files[file].filename)
        new_media = Media(api_key=api_key, file_name=file_name)
        db.session.add(new_media)
        db.session.commit()
        db.session.refresh(new_media)
        return jsonify(result="true", media_id=new_media.id), 201
    # if request.method == "POST":
    #     file = request.files["file"]
    #     filename = secure_filename(file.filename)
    #     file.save(os.path.join(app.config["MEDIA_FOLDER"], filename))
    # return """
    # <!doctype html>
    # <title>upload new File</title>
    # <form action="" method=post enctype=multipart/form-data>
    #   <p><input type=file name=file><input type=submit value=Upload>
    # </form>
    # """


if __name__ == "__main__":
    cli()
