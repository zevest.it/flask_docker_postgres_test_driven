from . import db
from typing import Dict, Any


class User(db.Model):
    __tablename__ = "users"

    id = db.Column(db.Integer, primary_key=True)
    api_key = db.Column(db.String(10), nullable=False)
    name = db.Column(db.String(50), nullable=False)
    # surname = db.Column(db.String(50), nullable=True)

    def to_json(self) -> Dict[str, Any]:
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}


class Tweet(db.Model):
    __tablename__ = 'tweets'

    id = db.Column(db.Integer, primary_key=True)
    tweet_data = db.Column(db.String(2000), nullable=False)
    tweet_media_ids = db.Column(db.String(2000), nullable=True)
    author_id = db.Column(db.Integer, db.ForeignKey('users.id'))
    author = db.relationship("User", backref="user_tweets")

    def to_json(self) -> Dict[str, Any]:
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}


class Like(db.Model):
    __tablename__ = 'likes'

    id = db.Column(db.Integer, primary_key=True)
    author_id = db.Column(db.Integer, db.ForeignKey('users.id'))
    author = db.relationship("User", backref="user_likes")
    tweet_id = db.Column(db.Integer, db.ForeignKey('tweets.id'))
    tweet = db.relationship("Tweet", backref="tweet_likes")

    def to_json(self) -> Dict[str, Any]:
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}


class Follower(db.Model):
    __tablename__ = 'followers'
    __table_args__ = (db.UniqueConstraint('follower', 'user_to_follow'),)

    id = db.Column(db.Integer, primary_key=True)
    follower = db.Column(db.ForeignKey('users.id'), nullable=False)
    user_to_follow = db.Column(db.ForeignKey('users.id'), nullable=False)


class Media(db.Model):
    __tablename__ = 'medias'
    id = db.Column(db.Integer, primary_key=True)
    api_key = db.Column(db.String(10), nullable=False)
    file_name = db.Column(db.String(30))
